const express = require('express')
const bodyParser = require('body-parser')
const connectDB = require('./config/db')
const app = express()
const PORT = process.env.PORT || 3333;


//Initiate Middelware
app.use(express.json())
app.use(bodyParser.urlencoded({ exrended: true }))
app.use(bodyParser.json())

//Conect DataBase
connectDB()

//Define Routes
app.use('/', require('./routes/hello'));
app.use('/user', require('./routes/api/user'));
app.use('/auth', require('./routes/api/auth'));

//Use Files Local or on server
app.get('*', (req, res)=>{
    res.sendFile(path.resolve(__dirname));
})

//Conect to port
app.listen(PORT, () => { console.log("Rodando") })