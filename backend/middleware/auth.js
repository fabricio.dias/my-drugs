const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = function(req, res, next) {
    const token = req.header('x-auth-token');
    if(!token){
        return res.status(401).json({msg: "No token, autorization denied"});
    }
    try {
        jwt.verify(token, config.get('jwtSecret'), (error, decoded) =>{
            if(error){
                return res.status(401).json({msg: "Toke ins not valid"});
            } else {
                req.user = decoded.user;
                next();
            }
        })
    } catch (err) {
        console.log('Something wrong with auth midleware');
        res.status(500).json({msg: "Server error"});
    }
}