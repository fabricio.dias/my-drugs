const express = require('express');
const router = express.Router();
const User = require('../../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const { check, validationResult } = require('express-validator');


router.post('/', [
    check('email', 'please include a valid email').isEmail(),
    check('senha', 'password is required').exists()
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors) {
        res.status(400).json({ errors: errors.array() });
    }
    const { email, senha } = req.body;
    try {
        let user = await User.findOne({ email }).select('id senha email');
        if (!user) {
            return res.status(404).json({ errors: ({ msg: "user not existis" }) });
        } else {
            const isMatch = await bcrypt.compare(senha, user.senha);
            if (!isMatch) {
                return res.status(400).json({ errors: ({ msg: "bad request, password is not valid" }) });
            } else {
                const payload = {
                    user: {
                        id: user.id
                    }
                }
                jwt.sign(payload, config.get('jwtSecret'), { expiresIn: '5 days' },
                    (err, token) => {
                        if (err) throw err;
                        res.json({ token })
                    }
                );
            }
        }
    } catch (error) {
        console.log(error.message);
        res.status(500).send("Server error");
    }
});

module.exports = router;