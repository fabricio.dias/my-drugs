const express = require('express')
const User = require('../../models/user');
const { check, validationResult } = require('express-validator');
const router = express.Router()
const bcrypt = require('bcryptjs');
const { response } = require('express');
const { findByIdAndUpdate } = require('../../models/User');
const auth = require('../../middleware/auth');

router.get('/:userId', [], async (req, res) => {
    try {
        const id = req.params.userId
        const user = await User.findOne({ _id: id })
        if (user) {
            res.json(user)
        }
        else {
            res.status(404).send({ "error": "user not found" })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": "Server Error" })
    }
})

//Private Router
router.get('/', auth, async (req, res, next) => {
    try {
        const user = await User.find({})
        res.json(user)
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": "Server Error" })
    }
})

router.delete('/:userId', async (req, res) => {
    try {
        const id = req.params.userId
        const user = await User.findOneAndDelete({ _id: id })
        if (user) {
            res.json(user)
        } else {
            res.status(404).send({ "error": "user not found" })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": "Server Error" })
    }
})


router.put('/:userId', [
    check('email', 'email is not valid').isEmail(),
    check('nome', 'name canot empty').not().isEmpty(),
    check('senha', 'password with 6 or more characters').isLength({ min: 6 })
], async (req, res) => {
    try {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: erros.array() })
        }

        const id = req.params.userId
        let { nome, email, senha, is_active, is_admin }
        const update = { nome, email, senha, is_active, is_admin } = req.body
        
        const salt = await bcrypt.genSalt(10)
        update.senha = await bcrypt.hash(senha, salt)
        
        let user = await User.findOneAndReplace({ _id: id }, update, { new: true })
        if (user) {
            res.json(user)
        } else {
            res.status(404).send({ "error": "user not found" })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": "Server Error" })
    }
})

router.patch('/:userId', [], async (req, res, next) => {
    try {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            res.status(400).send({ errors: errors.array() })
            return
        }
        const id = req.params.userId
        const salt = await bcrypt.genSalt(10)

        let bodyRequest = req.body

        if (bodyRequest.senha) {
            bodyRequest.senha = await bcrypt.hash(bodyRequest.senha, salt)
        }

        const update = { $set: bodyRequest }
        const user = await User.findByIdAndUpdate(id, update, { new: true })
        if (user) {
            res.send(user)
        } else {
            res.status(404).send({ "error": "user not exist" })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": "Server Error" })
    }
})

router.post('/', [
    check('email', 'email is not valid').isEmail(),
    check('nome', 'name canot empty').not().isEmpty(),
    check('senha', 'password with 6 or more characters').isLength({ min: 6 })
], async (req, res, next) => {
    try {
        let { nome, email, senha, is_active, is_admin } = req.body
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() })
        } else {
            let usuario = new User({ nome, email, senha, is_active, is_admin })
            const salt = await bcrypt.genSalt(10)
            usuario.senha = await bcrypt.hash('senha', salt)

            await usuario.save()
           
            if (usuario.id) {
                res.json(usuario)
            }
        }
    } catch (err) {
        console.log(err.message)
        res.status(500).send({ "error": "Server Error" })
    }
});

module.exports = router