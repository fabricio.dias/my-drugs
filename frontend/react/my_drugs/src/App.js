import React from 'react';
import Header from './components/header/header';
import Nav from './components/nav/nav';
import Footer from './components/footer/footer';


function App() {
  return (
    <div className="App">
      <Header></Header>
      <Nav></Nav>
      <Footer></Footer>
    </div>
  );
}

export default App;
