import { clientHttp } from '../config/config.js'

const createUser = (data) => clientHttp.post(`/api/user`, data)

const ListUser = () => clientHttp.get(`/api/user`)

export {
    createUser,
    ListUser
}