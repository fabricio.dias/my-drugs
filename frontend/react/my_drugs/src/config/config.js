import axios from 'axios'

const clientHttp = axios.create({
    baseURL: `htt://localhost/3000/api/user`,
})

clientHttp.defaults.headers['Content-Type'] = 'application/json';

export {
    clientHttp
}